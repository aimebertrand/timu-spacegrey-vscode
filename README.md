# README
## Timu Spacegrey Theme
VSCod(e|ium) colorful theme inspired by the [Spacegray](https://github.com/kkga/spacegray) theme for Sublime Text.

<p align="center"><img src="tsv-screenshot.png" width="100%"/></p>

## Installation
1. Press extensions icon in your editor.
2. Search for `aimebertrand.timu-spacegrey-vscode`.
3. Select this extension and press `install` button.

## Manual Installation
### VSCodium
Clone this Repo into your extensions directory:

- **Windows:**  `%USERPROFILE%\.vscode-oss\extensions`
- **macOS:**  `~/.vscode-oss/extensions`
- **Linux:**  `~/.vscode-oss/extensions`

### VSCode
Clone this Repo into your extensions directory:

- **Windows:**  `%USERPROFILE%\.vscode\extensions`
- **macOS:**  `~/.vscode/extensions`
- **Linux:**  `~/.vscode/extensions`

## License
MIT License
