# Change Log

All notable changes to the "timu-spacegrey-vscode" extension will be documented in this file.

## [1.0.0] - 2021-11-07

- Initial release

## [1.0.14] - 2021-11-08

- v1.0.1 broken/missing because of GitLab CI/CD issues
- Add CHANGELOG.md
- Adjust colors for Org-mode
- Adjust colors for Lisp

## [1.0.15] - 2021-11-10

- Add token colors for Ansible playbooks
- Add token colors for Swift

## [1.0.16] - 2021-11-11

- Update icon ("logo")

## [1.1.0] - 2021-11-16

- Refactor the theme JSON file

## [1.2.0] - 2023-01-14

- Update copyright year

## [1.2.1] - 2024-09-28

- Adjust Background Colour
- Adjust alternative Background Colour
